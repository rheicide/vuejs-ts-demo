import * as VueRouter from 'vue-router'

import Home from './components/home.vue'
import Posts from './components/posts.vue'
import Users from './components/users.vue'

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/posts',
      component: Posts
    },
    {
      path: '/users',
      component: Users
    }
  ]
})