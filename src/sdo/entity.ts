import { AxiosPromise } from 'axios'

interface Entity {
  id: number
  delete(): AxiosPromise
}

export default Entity