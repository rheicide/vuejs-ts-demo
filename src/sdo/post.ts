import axios from 'axios'
import { AxiosPromise } from 'axios'

class Post {
  static url = 'https://jsonplaceholder.typicode.com/posts'

  constructor (public id: number, public userId: number, public title: string, public body: string) {
    this.id = id;
    this.userId = userId;
    this.title = title;
    this.body = body;
  }

  delete(): AxiosPromise {
    return axios.delete(`${Post.url}/${this.id}`)
  }
}

export default Post;
