import axios from 'axios'
import { AxiosPromise } from 'axios'

class User {
  static url = 'https://jsonplaceholder.typicode.com/users'

  constructor (public id: number, public name: string, public username: string, public email: string) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
  }

  delete(): AxiosPromise {
    return axios.delete(`${User.url}/${this.id}`)
  }
}

export default User;
