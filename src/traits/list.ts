import { Vue, Trait, Lifecycle } from 'av-ts'
import axios from 'axios'
import Entity from '../sdo/entity'

@Trait
export default class List extends Vue {
  url: string = ''
  items: Entity[] = []
  fields = {}
  currentPage: number = 1
  perPage: number = 10
  toEntity: (obj: any) => Entity

  @Lifecycle created() {
    this.fetch()
  }

  fetch(): void {
    axios.get(this.url)
        .then((resp) => this.items = (resp.data as any[]).map(this.toEntity))
        .catch((err) => console.log(err))
  }

  remove(item: Entity): void {
    item.delete()
        .then(() => this.items = this.items.filter((it) => it.id != item.id))
        .catch((err) => console.log(err))
  }
}