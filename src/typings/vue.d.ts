declare module '*.vue' {
  import * as Vue from 'vue'
  const component: typeof Vue
  export default component
}