Feature: Homepage

Scenario: Access homepage

  Given I open home page
  Then the title is "Vue.js + TypeScript"
  And the container exists
