Feature: Posts page

Scenario: Posts should be loaded when the user visits the Posts page

  Given that a user is on the app
  When he clicks the Posts link on the navigation bar
  Then he should see the Posts page
  And there should be some posts loaded
