require('nightwatch-cucumber')({
  cucumberArgs: [
    '--require',
    'test/e2e/steps',
    '--format',
    'pretty',
    '--format',
    'json:test/e2e/reports/cucumber.json',
    'test/e2e/features'
  ]
})

module.exports = {
  output_folder: 'test/e2e/reports',

  selenium: {
    start_process: true,
    server_path: require('selenium-server').path,
    host: '127.0.0.1',
    port: 4444,
    cli_args: {
      'webdriver.chrome.driver': require('chromedriver').path
    }
  },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: 'localhost',
      silent: true,
      globals: {
        devServerURL: 'http://localhost:' + (process.env.PORT || 8080)
      },
      screenshots : {
        enabled : true,
        on_failure : true,
        path: 'test/e2e/reports/screenshots'
      }
    },

    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },

    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
  }
}
