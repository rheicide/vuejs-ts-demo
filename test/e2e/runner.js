var spawn = require('child_process').spawn;
var server = spawn('webpack-dev-server', ['--config', 'webpack.config.js'], { stdio: 'ignore' });

// Waiting webpack run dev server
setTimeout(function () {
  var nightWatchRunner = spawn('nightwatch', ['--config', 'test/e2e/nightwatch.conf.js', '--env', 'chrome'], { stdio: 'inherit' });

  function killServerAndGenerateReport() {
    server.kill();
    spawn('node', ['test/e2e/reporter.js'], { stdio: 'ignore' });
  }

  nightWatchRunner.on('exit', killServerAndGenerateReport);
  nightWatchRunner.on('error', killServerAndGenerateReport);
}, 2000);
