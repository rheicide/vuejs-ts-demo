const {client} = require('nightwatch-cucumber')
const {defineSupportCode} = require('cucumber')

defineSupportCode(({Given, Then, When}) => {
  Given(/^I open home page$/, () => {
    return client
      .url(client.globals.devServerURL)
      .waitForElementVisible('#app', 5000)
  })

  Then(/^the title is "(.*?)"$/, (text) => {
    return client.assert.containsText('h1', text)
  })

  Then(/^the container exists$/, () => {
    return client.assert.elementPresent('#home')
  })
})
