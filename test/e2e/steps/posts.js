const {client} = require('nightwatch-cucumber')
const {defineSupportCode} = require('cucumber')

defineSupportCode(({Given, Then, When}) => {
  Given(/^that a user is on the app$/, () => {
    return client
      .url(client.globals.devServerURL)
      .waitForElementVisible('#app', 5000)
  })

  When(/^he clicks the Posts link on the navigation bar$/, () => {
    return client.click('button.navbar-toggler').click('a[href="/posts"]')
  })

  Then(/^he should see the Posts page$/, () => {
    return client.assert.elementPresent('#posts')
  })

  Then(/^there should be some posts loaded$/, () => {
    return client.waitForElementNotVisible('#progress-blanket', 5000)
      .elements('css selector', '#posts table tbody tr', function (res) {
        this.assert.ok(res.value.length > 0)
      })
  })
})
