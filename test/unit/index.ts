import '../../src/plugins'

// Polyfill Promise for PhantomJS
global.Promise = require('es6-promise').Promise;

// Polyfill fn.bind() for PhantomJS
Function.prototype.bind = require('function-bind')

// require all test files (files that ends with .spec.js)
const testsContext = require.context('./specs', true, /\.spec$/)
testsContext.keys().forEach(testsContext)
