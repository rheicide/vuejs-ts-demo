module.exports = function (config) {
  config.set({
    // to run in additional browsers:
    // 1. install corresponding karma launcher
    //    http://karma-runner.github.io/0.13/config/browsers.html
    // 2. add it to the `browsers` array below.
    browsers: ['PhantomJS'],
    frameworks: ['mocha', 'sinon-chai'],
    reporters: ['spec'],
    files: ['./index.ts'],
    preprocessors: {
      './index.ts': ['webpack', 'sourcemap']
    },
    webpack: require("../../webpack.config"),
    webpackMiddleware: {
      noInfo: true
    },
    client: {
      captureConsole: true
    },
  })
}
