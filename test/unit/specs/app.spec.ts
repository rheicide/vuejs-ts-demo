import * as Vue from 'vue';
import {expect} from 'chai';
import App from '../../../src/components/app.vue';
import router from '../../../src/router';

describe('app.vue', () => {
  it('renders correctly', () => {
    const app = new App({router}).$mount();
    expect(app.$el.getAttribute('id')).to.equal("app");
  });
});
