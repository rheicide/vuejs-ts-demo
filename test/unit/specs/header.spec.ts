import * as Vue from 'vue';
import {expect} from 'chai';
import Header from '../../../src/components/header.vue';
import router from '../../../src/router';

describe('header.vue', () => {
  it('renders correctly', () => {
    const header = new Header({router}).$mount();
    expect(header.$el.querySelectorAll('.navbar-brand')).to.have.lengthOf(1);
  });
});
