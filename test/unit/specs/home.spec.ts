import * as Vue from 'vue';
import {expect} from 'chai';
import Home from '../../../src/components/home.vue';

describe('home.vue', () => {
  it('renders correctly', () => {
    const vm : any = new Home().$mount();
    expect(vm.msg).to.equal('Vue.js + TypeScript');
  });
});
