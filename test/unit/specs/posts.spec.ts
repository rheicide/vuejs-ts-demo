import * as Vue from 'vue';
import {expect} from 'chai';
import axios from 'axios';
import * as sinon from 'sinon/pkg/sinon';

import Posts from '../../../src/components/posts.vue';
import Post from '../../../src/sdo/post';

describe('posts.vue', () => {
  const postsTestingData = [
    new Post(1, 1, 'testing title 1', 'testing body 1'),
    new Post(2, 1, 'testing title 2', 'testing body 2')
  ];
  const resolved = new Promise((r) => r({data: postsTestingData}));

  let sandbox;
  let posts;

  before(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(axios, 'get').returns(resolved);
    posts = new Posts().$mount();
  });

  after(() => sandbox.restore());

  it('renders correctly', () => {
    expect(posts.$el.getAttribute('id')).to.equal('posts');
  });

  it('is created correctly', (done) => {
    setTimeout(() => {
      expect(posts.items).to.have.lengthOf(2);
      done();
    }, 200);
  });
});
