import * as Vue from 'vue';
import {expect} from 'chai';
import axios from 'axios';
import * as sinon from 'sinon/pkg/sinon';

import Users from '../../../src/components/users.vue';
import User from '../../../src/sdo/user';

describe('users.vue', () => {
  const usersTestingData = [
    new User(1, 'Name 1', 'name1', 'name1@email.com'),
    new User(2, 'Name 2', 'name2', 'name2@email.com')
  ];
  const resolved = new Promise((r) => r({data: usersTestingData}));

  let sandbox;
  let users;

  before(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(axios, 'get').returns(resolved);
    users = new Users().$mount();
  });

  after(() => sandbox.restore());

  it('renders correctly', () => {
    expect(users.$el.getAttribute('id')).to.equal('users');
  });

  it('is created correctly', (done) => {
    setTimeout(() => {
      expect(users.items).to.have.lengthOf(2);
      done();
    }, 200);
  });
});
